using ConsoleApp1;
using Moq;
using System;
using Xunit;

namespace XUnitTestProject1
{
    public class ProgramTests : IDisposable
    {
        private MockRepository mockRepository;



        public ProgramTests()
        {
            this.mockRepository = new MockRepository(MockBehavior.Strict);


        }

        public void Dispose()
        {            
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            // Cleanup
            this.mockRepository.VerifyAll();
        }

        private Program CreateProgram()
        {
            return new Program();
        }

        [Fact]
        public void TestMethod1()
        {
            // Arrange
            var unitUnderTest = CreateProgram();

            // Act
            
            // Assert
            Assert.NotNull(unitUnderTest);
        }

        [Fact]
        public void TestMethod2()
        {
            // Arrange
            var unitUnderTest = new Program.Oioi();

            // Act

            // Assert
            Assert.False(unitUnderTest.IsValid(2));
        }

        [Fact]
        public void TestMethod3()
        {
            // Arrange
            var unitUnderTest = new Program.Oioi();

            // Act

            // Assert
            Assert.False(unitUnderTest.IsInvalid(1));
        }

        [Fact]
        public void TestMethod4()
        {
            // Arrange
            var unitUnderTest = new Program.Oioi();

            // Act
            unitUnderTest.teste = 1;
            // Assert
            Assert.True(unitUnderTest.teste == 1);
        }
    }
}
